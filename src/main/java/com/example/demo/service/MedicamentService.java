package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Medicament;
import com.example.demo.repository.MedicamentRepository;


@Service
@Transactional
public class MedicamentService {

	@Autowired
	private MedicamentRepository medicamentRepository;

	public List<Medicament> findAll() {
		return medicamentRepository.findAll();
	}

	public Optional<Medicament> findById(Long id) {
		return medicamentRepository.findById(id);
	}

	public Medicament save(Medicament medicament) {
		return medicamentRepository.save(medicament);
	}

	public void deleteById(Long id) {
		medicamentRepository.deleteById(id);
	}

	public Medicament getMedicamentById(Long id) {
		Optional<Medicament> medicament = medicamentRepository.findById(id);

		if (medicament.isPresent()) {
			return medicament.get();
		}
		return null;
	}

}
