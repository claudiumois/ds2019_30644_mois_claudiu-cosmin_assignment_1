package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Infirmier;
import com.example.demo.repository.InfirmierRepository;


@Service
@Transactional
public class InfirmierService {

	@Autowired
	private InfirmierRepository infirmierRepository;

	public List<Infirmier> findAll() {
		return infirmierRepository.findAll();
	}

	public Optional<Infirmier> findById(Long id) {
		return infirmierRepository.findById(id);
	}

	public Infirmier save(Infirmier infirmier) {
		return infirmierRepository.save(infirmier);
	}

	public void deleteById(Long id) {
		infirmierRepository.deleteById(id);
	}

	public Infirmier getInfirmierById(Long id) {
		Optional<Infirmier> infirmier = infirmierRepository.findById(id);

		if (infirmier.isPresent()) {
			return infirmier.get();
		}
		return null;
	}
}
