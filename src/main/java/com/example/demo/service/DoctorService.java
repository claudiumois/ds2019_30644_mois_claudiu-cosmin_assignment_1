package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Doctor;
import com.example.demo.repository.DoctorRepository;


@Service
@Transactional
public class DoctorService {

	@Autowired
	private DoctorRepository doctorRepository;

	public List<Doctor> findAll() {
		return doctorRepository.findAll();
	}

	public Optional<Doctor> findById(Long id) {
		return doctorRepository.findById(id);
	}

	public Doctor save(Doctor Doctor) {
		return doctorRepository.save(Doctor);
	}

	public void deleteById(Long id) {
		doctorRepository.deleteById(id);
	}

	public Doctor getDoctorById(Long id) {
		Optional<Doctor> doctor = doctorRepository.findById(id);

		if (doctor.isPresent()) {
			return doctor.get();
		}
		return null;
	}
	
}
