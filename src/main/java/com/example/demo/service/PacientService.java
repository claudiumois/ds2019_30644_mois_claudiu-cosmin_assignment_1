package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Pacient;
import com.example.demo.repository.PacientRepository;

@Service
@Transactional
public class PacientService {

	@Autowired
	private PacientRepository pacientRepository;

	public List<Pacient> findAll() {
		return pacientRepository.findAll();
	}

	public Optional<Pacient> findById(Long id) {
		return pacientRepository.findById(id);
	}

	public Pacient save(Pacient pacient) {
		return pacientRepository.save(pacient);
	}

	public void deleteById(Long id) {
		pacientRepository.deleteById(id);
	}

	public Pacient getPacientById(Long id) {
		Optional<Pacient> pacient = pacientRepository.findById(id);

		if (pacient.isPresent()) {
			return pacient.get();
		}
		return null;
	}
}
