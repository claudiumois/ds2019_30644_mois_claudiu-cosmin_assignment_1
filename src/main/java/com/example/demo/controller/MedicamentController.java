package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.Medicament;
import com.example.demo.service.MedicamentService;


@Controller
@RequestMapping("/medicament")
public class MedicamentController {


	@Autowired
	private	MedicamentService medicamentService;
	
	private final static Logger log = LoggerFactory.getLogger(MedicamentController.class);
	
	@GetMapping("/find")
	public ResponseEntity<List<Medicament>> findAll(){
		return ResponseEntity.ok(medicamentService.findAll());

	}
	
	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Medicament medicament) {
		return ResponseEntity.ok(medicamentService.save(medicament));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Medicament> findById(@PathVariable Long id) {
		Optional<Medicament> medicament = medicamentService.findById(id);
		if (!medicament.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(medicament.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Medicament> update(@PathVariable Long id, @Valid @RequestBody Medicament medicament) {
		if (!medicamentService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		medicament.setIdMedicament(id.intValue());
		return ResponseEntity.ok(medicamentService.save(medicament));
	}
	
	@RequestMapping("/home")
	public String viewHomePage(Model model) {
		List<Medicament> listaMedicamente = medicamentService.findAll();
		model.addAttribute("listaMedicamente", listaMedicamente);
		return "home-page-medicament";
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteMedicament(Model model, @PathVariable(name = "id") long id) {
		medicamentService.deleteById(id);
		return "redirect:/medicament/home";
	}


	@RequestMapping(path = { "/edit", "/edit/{idMedicament}" })
	public String editMedicamentById(Model model, @PathVariable("idMedicament") Optional<Long> idMedicament) {

		if (idMedicament.isPresent()) {
			Medicament medicament = medicamentService.getMedicamentById(idMedicament.get());
			model.addAttribute("medicament", medicament);
		} else {
			model.addAttribute("medicament", new Medicament());
		}
		return "add-edit-medicament";
	}

	@RequestMapping(path = "/createMedicament", method = RequestMethod.POST)
	public String createOrUpdatemedicament(Medicament medicament) {
		medicamentService.save(medicament);
		return "redirect:/medicament/home";
	}
	
}
