package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.Infirmier;
import com.example.demo.service.InfirmierService;

@Controller
@RequestMapping("/infirmier")
public class InfirmierController {


	@Autowired
	private	InfirmierService infirmierService;
	
	private final static Logger log = LoggerFactory.getLogger(InfirmierController.class);
	
	@GetMapping("/find")
	public ResponseEntity<List<Infirmier>> findAll(){
		return ResponseEntity.ok(infirmierService.findAll());

	}
	
	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Infirmier infirmier) {
		return ResponseEntity.ok(infirmierService.save(infirmier));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Infirmier> findById(@PathVariable Long id) {
		Optional<Infirmier> infirmier = infirmierService.findById(id);
		if (!infirmier.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(infirmier.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Infirmier> update(@PathVariable Long id, @Valid @RequestBody Infirmier infirmier) {
		if (!infirmierService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		infirmier.setIdInfirmier(id.intValue());
		return ResponseEntity.ok(infirmierService.save(infirmier));
	}
	
	@RequestMapping("/home")
	public String viewHomePage(Model model) {
		List<Infirmier> listaInfirmieri = infirmierService.findAll();
		model.addAttribute("listaInfirmieri", listaInfirmieri);
		return "home-page-infirmier";
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteInfirmier(Model model, @PathVariable(name = "id") long id) {
		infirmierService.deleteById(id);
		return "redirect:/infirmier/home";
	}


	@RequestMapping(path = { "/edit", "/edit/{idInfirmier}" })
	public String editInfirmierById(Model model, @PathVariable("idInfirmier") Optional<Long> idInfirmier) {

		if (idInfirmier.isPresent()) {
			Infirmier infirmier = infirmierService.getInfirmierById(idInfirmier.get());
			model.addAttribute("infirmier", infirmier);
		} else {
			model.addAttribute("infirmier", new Infirmier());
		}
		return "add-edit-infirmier";
	}

	@RequestMapping(path = "/createInfirmier", method = RequestMethod.POST)
	public String createOrUpdateInfirmier(Infirmier infirmier) {
		infirmierService.save(infirmier);
		return "redirect:/infirmier/home";
	}
	
	
}
