package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/Spital")
public class PrincipalController {
	
	@RequestMapping("/home")
	public String viewHomePage(Model model) {
		return "principal";
	}
	
	@RequestMapping("/home2")
	public String viewHomePage2(Model model) {
		return "principalInfirmier";
	}
	
}
