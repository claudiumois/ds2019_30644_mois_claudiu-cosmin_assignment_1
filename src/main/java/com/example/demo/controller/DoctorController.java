package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.Doctor;
import com.example.demo.service.DoctorService;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	private	DoctorService doctorService;
	
	private final static Logger log = LoggerFactory.getLogger(DoctorController.class);
	
	@GetMapping("/find")
	public ResponseEntity<List<Doctor>> findAll(){
		return ResponseEntity.ok(doctorService.findAll());

	}
	
	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Doctor doctor) {
		return ResponseEntity.ok(doctorService.save(doctor));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Doctor> findById(@PathVariable Long id) {
		Optional<Doctor> doctor = doctorService.findById(id);
		if (!doctor.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(doctor.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Doctor> update(@PathVariable Long id, @Valid @RequestBody Doctor doctor) {
		if (!doctorService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		doctor.setIdDoctor(id.intValue());
		return ResponseEntity.ok(doctorService.save(doctor));
	}
	
	@RequestMapping("/home")
	public String viewHomePage(Model model) {
		List<Doctor> listaDoctori = doctorService.findAll();
		model.addAttribute("listaDoctori", listaDoctori);
		return "home-page-doctor";
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteDoctor(Model model, @PathVariable(name = "id") long id) {
		doctorService.deleteById(id);
		return "redirect:/doctor/home";
	}


	@RequestMapping(path = { "/edit", "/edit/{idDoctor}" })
	public String editDoctorById(Model model, @PathVariable("idDoctor") Optional<Long> idDoctor) {

		if (idDoctor.isPresent()) {
			Doctor doctor = doctorService.getDoctorById(idDoctor.get());
			model.addAttribute("doctor", doctor);
		} else {
			model.addAttribute("doctor", new Doctor());
		}
		return "add-edit-doctor";
	}

	@RequestMapping(path = "/createDoctor", method = RequestMethod.POST)
	public String createOrUpdateDoctor(Doctor doctor) {
		doctorService.save(doctor);
		return "redirect:/doctor/home";
	}

}
