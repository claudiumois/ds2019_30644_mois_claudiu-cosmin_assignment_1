package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.entity.Pacient;
import com.example.demo.service.PacientService;

@Controller
@RequestMapping("/pacient")
public class PacientController {

	@Autowired
	private	PacientService pacientService;
	
	private final static Logger log = LoggerFactory.getLogger(PacientController.class);
	
	@GetMapping("/find")
	public ResponseEntity<List<Pacient>> findAll(){
		return ResponseEntity.ok(pacientService.findAll());

	}
	
	@PostMapping("/create")
	public ResponseEntity create(@Valid @RequestBody Pacient pacient) {
		return ResponseEntity.ok(pacientService.save(pacient));
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Pacient> findById(@PathVariable Long id) {
		Optional<Pacient> pacient = pacientService.findById(id);
		if (!pacient.isPresent()) {
			log.error("Id" + id + "is not exists");
			ResponseEntity.badRequest().build();
		}

		return ResponseEntity.ok(pacient.get());
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Pacient> update(@PathVariable Long id, @Valid @RequestBody Pacient pacient) {
		if (!pacientService.findById(id).isPresent()) {
			log.error("Id " + id + " is not existed");
			ResponseEntity.badRequest().build();
		}

		pacient.setIdPacient(id.intValue());
		return ResponseEntity.ok(pacientService.save(pacient));
	}
	
	@RequestMapping("/home")
	public String viewHomePage(Model model) {
		List<Pacient> listaPacienti = pacientService.findAll();
		model.addAttribute("listaPacienti", listaPacienti);
		return "home-page-pacient";
	}
	
	@RequestMapping("/delete/{id}")
	public String deletePacient(Model model, @PathVariable(name = "id") long id) {
		pacientService.deleteById(id);
		return "redirect:/pacient/home";
	}


	@RequestMapping(path = { "/edit", "/edit/{idPacient}" })
	public String editPacientById(Model model, @PathVariable("idPacient") Optional<Long> idPacient) {

		if (idPacient.isPresent()) {
			Pacient pacient = pacientService.getPacientById(idPacient.get());
			model.addAttribute("pacient", pacient);
		} else {
			model.addAttribute("pacient", new Pacient());
		}
		return "add-edit-pacient";
	}

	@RequestMapping(path = "/createPacient", method = RequestMethod.POST)
	public String createOrUpdatePacient(Pacient pacient) {
		pacientService.save(pacient);
		return "redirect:/pacient/home";
	}
	
}
