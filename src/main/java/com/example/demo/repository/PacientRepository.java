package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Pacient;

@Repository
public interface PacientRepository extends JpaRepository<Pacient, Long>{

}
