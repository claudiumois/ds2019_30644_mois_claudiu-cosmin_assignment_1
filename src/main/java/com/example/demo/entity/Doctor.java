package com.example.demo.entity;

import java.time.LocalTime;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Doctor")
public class Doctor {

	@Column(name = "idDoctor")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idDoctor;

	@Column(name = "name")
	private String name;

	@Column(name = "birthDate")
	private Date birthDate;

	@Column(name = "gender")
	private String gender;
	
	@OneToMany(mappedBy="doctor")
	private List<Pacient> listaPacienti;

	public Doctor() {

	}

	public Doctor(long idDoctor, String name, Date birthDate, String gender, List<Pacient> listaPacienti) {
		super();
		this.idDoctor = idDoctor;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.listaPacienti = listaPacienti;
	}

	public long getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(long idDoctor) {
		this.idDoctor = idDoctor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<Pacient> getListaPacienti() {
		return listaPacienti;
	}

	public void setListaPacienti(List<Pacient> listaPacienti) {
		this.listaPacienti = listaPacienti;
	}

}
