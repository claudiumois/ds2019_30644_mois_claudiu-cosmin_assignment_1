package com.example.demo.entity;

import java.time.LocalTime;
import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Pacient")
public class Pacient {

	@Column(name = "idPacient")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idPacient;

	@Column(name = "name")
	private String name;

	@Column(name = "birthDate")
	private Date birthDate;

	@Column(name = "gender")
	private String gender;

	@Column(name = "address")
	private String address;

	@ManyToMany(mappedBy = "listaPacienti", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JsonBackReference
	private List<Infirmier> listaInfirmieri;

	@Fetch(FetchMode.SELECT)
	@ManyToMany
	@JoinTable(name = "Medicament_Pacient", joinColumns = @JoinColumn(name = "idPacient"), inverseJoinColumns = @JoinColumn(name = "idMedicament"))
	@JsonManagedReference
	private List<Medicament> listaMedicamente;

	@ManyToOne
	@JoinColumn(name = "idDoctor", nullable = false)
	private Doctor doctor;

	public Pacient() {
	}

	public Pacient(long idPacient, String name, Date birthDate, String gender, String address,Doctor doctor,
			List<Medicament> listaMedicamente) {
		super();
		this.idPacient = idPacient;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.doctor = doctor;
		this.listaMedicamente = listaMedicamente;
	}

	public long getIdPacient() {
		return idPacient;
	}

	public void setIdPacient(long idPacient) {
		this.idPacient = idPacient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public List<Medicament> getListaMedicamente() {
		return listaMedicamente;
	}

	public void setListaMedicamente(List<Medicament> listaMedicamente) {
		this.listaMedicamente = listaMedicamente;
	}

}
