package com.example.demo.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Medicament")
public class Medicament {

	@Column(name = "idMedicament")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idMedicament;

	@Column(name = "name")
	private String name;

	@Column(name = "effect")
	private String effect;

	@Column(name = "dosage")
	private float dosage;

	@ManyToMany(mappedBy = "listaMedicamente", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JsonBackReference
	List<Pacient> listaPacienti;

	public Medicament() {

	}

	public Medicament(long idMedicament, String name, String effect, float dosage, List<Pacient> listaPacienti) {
		super();
		this.idMedicament = idMedicament;
		this.name = name;
		this.effect = effect;
		this.dosage = dosage;
		this.listaPacienti = listaPacienti;
	}

	public long getIdMedicament() {
		return idMedicament;
	}

	public void setIdMedicament(long idMedicament) {
		this.idMedicament = idMedicament;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public float getDosage() {
		return dosage;
	}

	public void setDosage(float dosage) {
		this.dosage = dosage;
	}

	public List<Pacient> getListaPacienti() {
		return listaPacienti;
	}

	public void setListaPacienti(List<Pacient> listaPacienti) {
		this.listaPacienti = listaPacienti;
	}

	
}
