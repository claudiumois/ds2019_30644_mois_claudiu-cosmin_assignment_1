package com.example.demo.entity;

import java.time.LocalTime;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Infirmier")
public class Infirmier {

	@Column(name = "idInfirmier")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idInfirmier;

	@Column(name = "name")
	private String name;

	@Column(name = "birthDate")
	private Date birthDate;

	@Column(name = "gender")
	private String gender;

	@Column(name = "address")
	private String address;

	@Fetch(FetchMode.SELECT)
	@ManyToMany
	@JoinTable(name = "Infirmier_Pacient", joinColumns = @JoinColumn(name = "idInfirmier"), inverseJoinColumns = @JoinColumn(name = "idPacient"))
	@JsonManagedReference
	private List<Pacient> listaPacienti;

	public Infirmier() {

	}

	public Infirmier(long idInfirmier, String name, Date birthDate, String gender, String address,
			List<Pacient> listaPacienti) {
		super();
		this.idInfirmier = idInfirmier;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.listaPacienti = listaPacienti;
	}

	public long getIdInfirmier() {
		return idInfirmier;
	}

	public void setIdInfirmier(long idInfirmier) {
		this.idInfirmier = idInfirmier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Pacient> getListaPacienti() {
		return listaPacienti;
	}

	public void setListaPacienti(List<Pacient> listaPacienti) {
		this.listaPacienti = listaPacienti;
	}

	
}
